package com.digisafari.sapl.user.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class User {

	@Id
	private String id;
	private String name;
	private String email;
	private String username;
	private String password;
	private String termsAccepted;
	private String createdOn;
	private String updatedOn;
	private String userRole;
	private String totalScore;
	private String userScore;
	private AttemptedQuizzes attemptedQuizzes;
	private String attemptedOn;
	private UserProfile userProfile;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTermsAccepted() {
		return termsAccepted;
	}
	public void setTermsAccepted(String termsAccepted) {
		this.termsAccepted = termsAccepted;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}
	public String getUserScore() {
		return userScore;
	}
	public void setUserScore(String userScore) {
		this.userScore = userScore;
	}
	public AttemptedQuizzes getAttemptedQuizzes() {
		return attemptedQuizzes;
	}
	public void setAttemptedQuizzes(AttemptedQuizzes attemptedQuizzes) {
		this.attemptedQuizzes = attemptedQuizzes;
	}
	public String getAttemptedOn() {
		return attemptedOn;
	}
	public void setAttemptedOn(String attemptedOn) {
		this.attemptedOn = attemptedOn;
	}
	public UserProfile getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email + ", username=" + username + ", password="
				+ password + ", termsAccepted=" + termsAccepted + ", createdOn=" + createdOn + ", updatedOn="
				+ updatedOn + ", userRole=" + userRole + ", totalScore=" + totalScore + ", userScore=" + userScore
				+ ", attemptedQuizzes=" + attemptedQuizzes + ", attemptedOn=" + attemptedOn + ", userProfile="
				+ userProfile + "]";
	}
	
	

}
