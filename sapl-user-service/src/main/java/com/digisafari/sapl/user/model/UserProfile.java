package com.digisafari.sapl.user.model;

public class UserProfile {

private String id;
private String gender;
private String dob;
private String avaterUrl;
private String profession;
private String contactNo;
private String educationHistory;
private String linkedinUrl;
private String proficiency;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getDob() {
return dob;
}

public void setDob(String dob) {
this.dob = dob;
}

public String getAvaterUrl() {
return avaterUrl;
}

public void setAvaterUrl(String avaterUrl) {
this.avaterUrl = avaterUrl;
}

public String getProfession() {
return profession;
}

public void setProfession(String profession) {
this.profession = profession;
}

public String getContactNo() {
return contactNo;
}

public void setContactNo(String contactNo) {
this.contactNo = contactNo;
}

public String getEducationHistory() {
return educationHistory;
}

public void setEducationHistory(String educationHistory) {
this.educationHistory = educationHistory;
}

public String getLinkedinUrl() {
return linkedinUrl;
}

public void setLinkedinUrl(String linkedinUrl) {
this.linkedinUrl = linkedinUrl;
}

public String getProficiency() {
return proficiency;
}

public void setProficiency(String proficiency) {
this.proficiency = proficiency;
}

@Override
public String toString() {
	return "UserProfile [id=" + id + ", gender=" + gender + ", dob=" + dob + ", avaterUrl=" + avaterUrl
			+ ", profession=" + profession + ", contactNo=" + contactNo + ", educationHistory=" + educationHistory
			+ ", linkedinUrl=" + linkedinUrl + ", proficiency=" + proficiency + "]";
}



}