package com.digisafari.sapl.user.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digisafari.sapl.user.exception.UserAlreadyExistsException;
import com.digisafari.sapl.user.exception.UserNotFoundException;
import com.digisafari.sapl.user.model.User;
import com.digisafari.sapl.user.repository.UserRepository;


@Service
public class UserService implements IUserService {

	private UserRepository userRepository;
	
	
	
	@Autowired
	public UserService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public boolean validateUser(String username, String password) throws UserNotFoundException {
		
		User user = userRepository.validateUser(username, password);
		System.out.println("user" + user);
		if(user != null)
			return true;
		else
			return false;
	}

	@Override
	public User getUserByName(String name) throws UserNotFoundException {
		return userRepository.findByname(name);
	}
	
	@Override
	public User getUserByUsername(String username) throws UserNotFoundException {
		return userRepository.findByusername(username);
	}
	
	@Override
	public User getUserById(String userId) throws UserNotFoundException {
		User user = null;
		try {
			Optional<User> userById = this.userRepository.findById(userId);
			if(userById.isPresent()) {
				user = userById.get();
			} else {
				throw new UserNotFoundException();
			}
		} catch (UserNotFoundException e) {
			throw e;
		} catch(Exception e) {
			System.out.println("inside exception: " + e.getMessage());
		}
		return user;
	}

	@Override
	public List<User> getAllUsers() {
		
		return userRepository.findAll();
	}

	@Override
	public User saveUser(User user) throws UserAlreadyExistsException {
		
		Optional<User> optional = this.userRepository.findById(user.getId());
		if(optional.isPresent()) {
			throw new UserAlreadyExistsException();
		}
		
			User createdUser = userRepository.save(user);
		
		
		return createdUser;
	}

	@Override
	public boolean deleteUser(String userId) throws UserNotFoundException {
		boolean deleteStatus = false;
		try {
			Optional<User> userById = this.userRepository.findById(userId);
			if(userById.isPresent()) {
				this.userRepository.deleteById(userId);
				deleteStatus = true;
			} else {
				throw new UserNotFoundException();
			}
		} catch (UserNotFoundException e) {
			throw e;
		} catch(Exception e) {
			
		}
		return deleteStatus;
	}
	
	
	@Override
	public User updateUser(User user) throws UserNotFoundException {
		try {
			Optional<User> userById = this.userRepository.findById(user.getId());
			if(userById.isPresent()) {
				this.userRepository.save(user);
			} else {
				throw new UserNotFoundException();
			}
		} catch (UserNotFoundException e) {
			throw e;
		} catch(Exception e) {
			System.out.println("inside exception: " + e.getMessage());
		}
		return user;
	}

}
