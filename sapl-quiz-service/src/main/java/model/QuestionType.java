package model;

public class QuestionType {

	private Boolean MCQ;
	
	// no argument constructor 
	public QuestionType(Boolean MCQ) {
		this.MCQ = MCQ;
	}

	/**
	 * @return the mCQ
	 */
	public Boolean getMCQ() {
		return MCQ;
	}

	/**
	 * @param mCQ the mCQ to set
	 */
	public void setMCQ(Boolean mCQ) {
		MCQ = mCQ;
	}

	// To string() begin

	@Override
	public String toString() {
		return "QuestionType [MCQ=" + MCQ + "]";
	}
	
	// To string() end

	
}
