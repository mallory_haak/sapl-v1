package service;

import java.util.List;

import excepetion.QuizAlreadyExistsException;
import excepetion.QuizNotFoundException;
import model.Quiz;

public interface IQuizService {

	public Quiz createNewQuiz(Quiz quiz) throws QuizAlreadyExistsException;
	public List<Quiz> getQuizList();
	public Quiz updateQuiz(Quiz quiz) throws QuizNotFoundException;
	public Quiz getQuizById(String id) throws QuizNotFoundException;
	public boolean deleteQuiz(String id) throws QuizNotFoundException;
}
